################################################################################
#
# faust
#
################################################################################

FAUST_VERSION = 2.30.5
FAUST_SITE = https://github.com/grame-cncm/faust/releases/download/$(FAUST_VERSION)
FAUST_LICENSE = GPL2
FAUST_LICENSE_FILES = COPYING.txt
HOST_FAUST_DEPENDENCIES = host-cmake

define HOST_FAUST_BUILD_CMDS
	$(HOST_MAKE_ENV) \
		$(MAKE) PREFIX=/usr -C $(@D)
endef

define HOST_FAUST_INSTALL_CMDS
	$(HOST_MAKE_ENV) \
		$(MAKE) PREFIX=/usr DESTDIR=$(HOST_DIR) -C $(@D) install
endef

$(eval $(host-generic-package))
