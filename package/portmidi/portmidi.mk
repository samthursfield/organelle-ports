################################################################################
#
# portmidi
#
################################################################################

PORTMIDI_VERSION = 217
PORTMIDI_SOURCE = portmidi-src-$(PORTMIDI_VERSION).zip
PORTMIDI_SITE = https://sourceforge.net/projects/portmedia/files/portmidi/$(PORTMIDI_VERSION)
PORTMIDI_INSTALL_STAGING = YES

PORTMIDI_CONF_OPTS = \
	-DINCLUDE_INSTALL_DIR=/usr/include \
	-DLIB_INSTALL_DIR=/usr/lib \
	-DJAVA_INCLUDE_PATH=none \
	-DJAVA_INCLUDE_PATH2=none \
	-DJAVA_JVM_LIB_PATH2=none \
	-DJAVA_JVM_LIBRARY=none

define PORTMIDI_EXTRACT_CMDS
	$(UNZIP) $(PORTMIDI_DL_DIR)/$(PORTMIDI_SOURCE) -d $(@D)
	mv $(@D)/portmidi/* $(@D)
endef

$(eval $(cmake-package))
