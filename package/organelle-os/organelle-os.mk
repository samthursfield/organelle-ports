################################################################################
#
# organelle-os
#
################################################################################

ORGANELLE_OS_VERSION = 0764cf9bac2ce438232e15da946643b3b1460501
ORGANELLE_OS_SITE = git://github.com/critterandguitari/Organelle_OS
ORGANELLE_OS_LICENSE = BSD-3-Clause
ORGANELLE_OS_LIBFOO_LICENSE_FILES = LICENSE.txt

ifeq ($(BR2_PACKAGE_SYSTEMD),y)
ORGANELLE_OS_DEPENDENCIES = systemd
endif

define ORGANELLE_OS_BUILD_CMDS
	$(MAKE) $(TARGET_CONFIGURE_OPTS) -C $(@D) organelle
endef

define ORGANELLE_OS_INSTALL_TARGET_CMDS
	$(INSTALL) -d -m 0755 $(TARGET_DIR)/usr/lib/organelle
	cp -a $(@D)/fw_dir $(TARGET_DIR)/usr/lib/organelle/
endef

define ORGANELLE_OS_INSTALL_INIT_SYSTEMD
	$(INSTALL) -D -m 0644 package/organelle-os/organelle-os.service \
	        $(TARGET_DIR)/usr/lib/systemd/system/organelle-os.service
endef

$(eval $(generic-package))
