################################################################################
#
# boucle
#
################################################################################

BOUCLE_VERSION = a70ce1b31d0485baec41a5dca3d2fd0bf08e2b6b
BOUCLE_SOURCE = $(BOUCLE_VERSION).tar.gz
BOUCLE_SITE = https://github.com/ssssam/boucle/archive
BOUCLE_LICENSE = GPL-3.0+
BOUCLE_LICENSE_FILES = COPYING

BOUCLE_DEPENDENCIES = alsa-lib host-rustc portmidi

BOUCLE_CARGO_ENV = CARGO_HOME=$(HOST_DIR)/share/cargo

BOUCLE_BIN_DIR = target/$(RUSTC_TARGET_NAME)/$(BOUCLE_CARGO_MODE)

BOUCLE_CARGO_OPTS = \
	$(if $(BR2_ENABLE_DEBUG),,--release) \
	--target=$(RUSTC_TARGET_NAME) \
	--manifest-path=$(@D)/Cargo.toml

BOUCLE_INSTALL_DIR = /sdcard/Patches/boucle

define BOUCLE_BUILD_CMDS
	$(TARGET_MAKE_ENV) $(BOUCLE_CARGO_ENV) \
	        PKG_CONFIG_ALLOW_CROSS=1 \
	        cargo build $(BOUCLE_CARGO_OPTS)
endef

define BOUCLE_INSTALL_TARGET_CMDS
	$(INSTALL) -d $(TARGET_DIR)/$(BOUCLE_INSTALL_DIR)
	$(INSTALL) -m 0755 $(@D)/$(BOUCLE_BIN_DIR)/release/boucle_organelle \
	        $(TARGET_DIR)$(BOUCLE_INSTALL_DIR)
	$(INSTALL) -m 0755 $(@D)/organelle/data/run.sh \
	        $(TARGET_DIR)$(BOUCLE_INSTALL_DIR)
endef

$(eval $(generic-package))
