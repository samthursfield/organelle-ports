################################################################################
#
# python-pyliblo
#
################################################################################

PYTHON_PYLIBLO_VERSION = 0.10.0
PYTHON_PYLIBLO_SOURCE = pyliblo-$(PYTHON_PYLIBLO_VERSION).tar.gz
PYTHON_PYLIBLO_SITE = http://das.nasophon.de/download
PYTHON_PYLIBLO_SETUP_TYPE = setuptools
PYTHON_PYLIBLO_LICENSE = LGPL-2.1
PYTHON_PYLIBLO_LICENSE_FILES = COPYING

$(eval $(python-package))
