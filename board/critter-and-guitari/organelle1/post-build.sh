#!/bin/sh

BOARD_DIR="$(dirname $0)"

install -d -m 755 $TARGET_DIR/boot

install -d -m 755 $TARGET_DIR/usbdrive

$HOST_DIR/bin/mkimage -A arm -O linux -T script -C none  \
	-n "boot script" -d $BOARD_DIR/boot.scr.txt $TARGET_DIR/boot/boot.scr

# Enable sudo for `organelle` user and anyone else in group `wheel`
sed -i $TARGET_DIR/etc/sudoers -e 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/'
