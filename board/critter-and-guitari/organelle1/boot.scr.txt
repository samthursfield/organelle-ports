setenv finduuid "part uuid mmc 1:1 uuid"
run finduuid
run findfdt
setenv bootargs "root=PARTUUID=${uuid} rootwait ro rootfstype=ext4 console=tty0 consoleblank=0 video=mxcfb0:dev=hdmi,1920x1080M@60,if=RGB24,bpp=16 dmfc=3 ahci_imx.hotplug=1 pci=nomsi loop.max_part=15 fsck.mode=skip"
load mmc 1:1 ${fdt_addr} boot/${fdtfile}
load mmc 1:1 ${loadaddr} boot/zImage
bootz ${loadaddr} - ${fdt_addr}
