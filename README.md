# Buildroot for Organelle

This produces a small rootfs that can run on the Organelle 1.

![](organelle-1.jpg)

**Buildroot for Organelle is a prototype with no guarantees of any sort.**

It doesn't do anything useful on its own, but allows you to build some interesting
custom firmwares. See the upstream [Organelle_OS](https://github.com/critterandguitari/Organelle_OS)
repo for how you can use OpenSoundControl to interface with the hardware.

## Build instructions

Quick start:

    make organelle_1_defconfig
    make

See Buildroot documentation for more information:
https://buildroot.org/docs.html

## Deployment instructions

Copy the `sdcard.img` to the SDcard, as described in
[section 7 of the Organelle 1 manual](https://www.critterandguitari.com/manual?m=Organelle_Manual#71-burning-sd-card-disk-image).

Power on the device **with HDMI cable unplugged**. Then connect the HDMI cable.
